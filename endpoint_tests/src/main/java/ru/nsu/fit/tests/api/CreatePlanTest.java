package ru.nsu.fit.tests.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.data.Customer;
import ru.nsu.fit.data.Plan;
import ru.nsu.fit.data.ResponseMessage;
import ru.nsu.fit.services.log.Logger;

import java.io.IOException;

public class CreatePlanTest extends ParentTest {

    private Plan planBeforeCreateMethod;

    @BeforeClass(description = "")
    public void beforeTests() {
        planBeforeCreateMethod = new Plan()
                .setName("Greatest Things")
                .setDetails("Greatest Things details")
                .setFee(200);
    }

    @Test(description = "Create plan via API.")
    public void createPlan() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(planBeforeCreateMethod);

        String responseText = sendJsonRequest("create_plan", "admin", "setup",
                "post", json);
        Plan planAfterCreateMethod = mapper.readValue(responseText, Plan.class);
        planAfterCreateMethod.setId(planBeforeCreateMethod.getId());

        Logger.debug("planBeforeCreateMethod:\n"+planBeforeCreateMethod.toString());
        Logger.debug("planAfterCreateMethod:\n"+planAfterCreateMethod.toString());
        Assert.assertTrue(planAfterCreateMethod.equals(planBeforeCreateMethod));
        Logger.info("\n");
    }

    @Test(description = "Create plan via API second time.", dependsOnMethods = "createPlan")
    public void createPlan2() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(planBeforeCreateMethod);

        String responseText = sendJsonRequest("create_plan", "admin", "setup",
                "post", json);
        ResponseMessage respMessage = mapper.readValue(responseText, ResponseMessage.class);
        Assert.assertEquals("Plan with such name already exists.", respMessage.getMessage());
        Logger.info("\n");
    }

    /*@Test(description = "Subscribe to plan", dependsOnMethods = "login")
    public void customerSubscribeToPlan(){
        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("helloworld123@login.com", "password123");
        clientConfig.register( feature) ;

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest").path("create_subscription");
        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Logger.debug("Try to make POST...");
        Response response = invocationBuilder.post(Entity.entity("{\n" +
                "\t\"customer_id\":\"\",\n" +
                "    \"plan_id\":\"\",\n" +
                "}", MediaType.APPLICATION_JSON));
        Logger.info("Response: " + response.readEntity(String.class));

    }

    @Test(description = "Subscribe to the same plan second time", dependsOnMethods = "customerSubscribeToPlan")
    public void customerSubscribeToPlan2(){
        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("helloworld123@login.com", "password123");
        clientConfig.register( feature) ;

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest").path("create_subscription");
        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Logger.debug("Try to make POST...");
        Response response = invocationBuilder.post(Entity.entity("{\n" +
                "\t\"customer_id\":\"\",\n" +
                "    \"plan_id\":\"\",\n" +
                "}", MediaType.APPLICATION_JSON));
        Logger.info("Response: " + response.readEntity(String.class));
    }*/

    @Test(description = "Remove created plan.", dependsOnMethods = "createPlan2")
    public void removeCustomer() throws IOException {
        String responseText = sendJsonRequest("remove_plan/"+planBeforeCreateMethod.getName(), "admin",
                "setup", "post", "");

        ObjectMapper mapper = new ObjectMapper();
        ResponseMessage respMessage = mapper.readValue(responseText, ResponseMessage.class);

        Assert.assertEquals("Plan "+planBeforeCreateMethod.getName()+" was removed.", respMessage.getMessage());
    }

}
