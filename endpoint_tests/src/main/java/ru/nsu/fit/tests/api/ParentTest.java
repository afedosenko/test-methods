package ru.nsu.fit.tests.api;

import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import ru.nsu.fit.services.log.Logger;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class ParentTest {

    protected String sendJsonRequest(String path, String username, String password, String method, String json){
        ClientConfig clientConfig = new ClientConfig();
        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic(username, password);
        clientConfig.register( feature) ;
        clientConfig.register(JacksonFeature.class);
        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest").path(path);
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        Response response = null;
        switch (method) {
            case "get": {
                Logger.info("Try to make GET " + path);
                Logger.info("Authorization - Username: "+username+", Password: "+password);
                Logger.info("Body: "+json);
                response = invocationBuilder.post(Entity.entity(json, MediaType.APPLICATION_JSON));
                break;
            } case "post": {
                Logger.info("Try to make POST " + path);
                Logger.info("Authorization - Username: "+username+", Password: "+password);
                Logger.info("Body: "+json);
                response = invocationBuilder.post(Entity.entity(json, MediaType.APPLICATION_JSON));
                break;
            }
        }
        String responseText = null;
        if (response != null) {
            responseText = response.readEntity(String.class);
            Logger.info("Response: " + responseText);
        } else {
            Logger.info("Response can't be displayed");
        }
        return responseText; //there's strange error "java.lang.IllegalStateException: Entity input stream has already been closed."
                             //when I return Response and try to read it in other methods
    }

}
