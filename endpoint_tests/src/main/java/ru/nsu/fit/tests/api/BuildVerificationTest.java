package ru.nsu.fit.tests.api;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.testng.annotations.Test;
import ru.nsu.fit.services.log.Logger;

import javax.ws.rs.client.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.lang.reflect.Type;
import java.util.Map;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class BuildVerificationTest {
    @Test(description = "Create customer via API.")
    public void createCustomer() {
        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("admin", "setup");
        clientConfig.register( feature) ;

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest").path("create_customer");

        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Logger.debug("Try to make POST...");
        Response response = invocationBuilder.post(Entity.entity("{\n" +
                "\t\"firstName\":\"Johnds\",\n" +
                "    \"lastName\":\"Weak\",\n" +
                "    \"login\":\"helloworld123@login.com\",\n" +
                "    \"pass\":\"password123\",\n" +
                "    \"balance\":\"100\"\n" +
                "}", MediaType.APPLICATION_JSON));
        Logger.info("Response: " + response.readEntity(String.class));
    }

    @Test(description = "Try to log into the system.", dependsOnMethods = "createCustomer")
    public void login() {
        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("helloworld123@login.com", "password123");
        clientConfig.register( feature) ;

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest").path("get_customer/helloworld123@login.com");
        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Logger.debug("Try to make GET...");
        Response response = invocationBuilder.get();
        Logger.info("Response: " + response.readEntity(String.class));
    }

    @Test(description = "Create plan")
    public void createPlan(){
        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("admin", "setup");
        clientConfig.register( feature) ;

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );


        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest").path("create_plan");
        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Logger.debug("Try to make POST...");
        Response response = invocationBuilder.post(Entity.entity("{\n" +
                "    \"details\":\"Greatest Things\",\n" +
                "    \"fee\":\"100\",\n" +
                "}", MediaType.APPLICATION_JSON));
        String responseText = response.readEntity(String.class);
        Logger.info("Response: " + responseText);
    }
    @Test(description = "Subscribe to plan", dependsOnMethods = "login")
    public void cutomerSubscribeToPlan(){
        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("helloworld123@login.com", "password123");
        clientConfig.register( feature) ;

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest").path("get_customer/helloworld123@login.com");
        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Logger.debug("Try to make GET...");
        Response response = invocationBuilder.get();

        Type type = new TypeToken<Map<String, String>>(){}.getType();
        Map<String, String> customer = new Gson().fromJson(response.readEntity(String.class), type);
        String customerId = customer.get("id");

        webTarget = client.target("http://localhost:8080/endpoint/rest").path("get_plan/GreatestThings");
        invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Logger.debug("Try to make GET...");
        response = invocationBuilder.get();

        Map<String, String> plan = new Gson().fromJson(response.readEntity(String.class), type);
        String planId = plan.get("id");

        webTarget = client.target("http://localhost:8080/endpoint/rest").path("create_subscription");
        invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Logger.debug("Try to make POST...");
        response = invocationBuilder.post(Entity.entity("{\n" +
                "\t\"customer_id\":\"" + customerId +"\",\n" +
                "    \"plan_id\":\"" + planId + "\",\n" +
                "}", MediaType.APPLICATION_JSON));
        Logger.info("Response: " + response.readEntity(String.class));

    }

    @Test(description = "Subscribe to the same plan second time", dependsOnMethods = "cutomerSubscribeToPlan")
    public void cutomerSubscribeToPlan2(){
        ClientConfig clientConfig = new ClientConfig();

        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("helloworld123@login.com", "password123");
        clientConfig.register( feature) ;

        clientConfig.register(JacksonFeature.class);

        Client client = ClientBuilder.newClient( clientConfig );

        WebTarget webTarget = client.target("http://localhost:8080/endpoint/rest").path("create_subscription");
        Invocation.Builder invocationBuilder =	webTarget.request(MediaType.APPLICATION_JSON);
        Logger.debug("Try to make POST...");
        Response response = invocationBuilder.post(Entity.entity("{\n" +
                "\t\"customer_id\":\"\",\n" +
                "    \"plan_id\":\"\",\n" +
                "}", MediaType.APPLICATION_JSON));
        Logger.info("Response: " + response.readEntity(String.class));
    }
}
