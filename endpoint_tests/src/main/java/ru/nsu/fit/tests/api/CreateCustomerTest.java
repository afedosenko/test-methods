package ru.nsu.fit.tests.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.nsu.fit.data.Customer;
import ru.nsu.fit.data.ResponseMessage;
import ru.nsu.fit.services.log.Logger;

import java.io.IOException;

/**
 * @author Timur Zolotuhin (tzolotuhin@gmail.com)
 */
public class CreateCustomerTest extends ParentTest {

    private Customer customerBeforeCreateMethod;

    @BeforeClass(description = "")
    public void beforeTests() {
        customerBeforeCreateMethod = new Customer()
                .setFirstName("Johnds")
                .setLastName("Weak")
                .setLogin("helloworld123@login.com")
                .setPass("password123")
                .setBalance(200);
    }

    @Test(description = "Create customer via API.")
    public void createCustomer() throws IOException { //is it okay to make throws here?
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(customerBeforeCreateMethod);

        String responseText = sendJsonRequest("create_customer", "admin", "setup",
                "post", json);
        Customer customerAfterCreateMethod = mapper.readValue(responseText, Customer.class);
        customerAfterCreateMethod.setId(customerBeforeCreateMethod.getId());

        Logger.debug("customerBeforeCreateMethod:\n"+customerBeforeCreateMethod.toString());
        Logger.debug("customerAfterCreateMethod:\n"+customerAfterCreateMethod.toString());
        Assert.assertTrue(customerAfterCreateMethod.equals(customerBeforeCreateMethod));
        Logger.info("\n");
    }

    @Test(description = "Create customer via API second time.", dependsOnMethods = "createCustomer")
    public void createCustomer2() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(customerBeforeCreateMethod);

        String responseText = sendJsonRequest("create_customer", "admin", "setup",
                "post", json);
        ResponseMessage respMessage = mapper.readValue(responseText, ResponseMessage.class);
        Assert.assertEquals("Customer with such login already exists.", respMessage.getMessage());
        Logger.info("\n");
    }

    /*@Test(description = "Try to log into the system and get oneself's id.", dependsOnMethods = "createCustomer")
    public void getCustomerId() throws IOException {
        String responseText = sendJsonRequest("get_customer_id/helloworld123@login.com",
                "helloworld123@login.com", "password123", "get", "");

        ObjectMapper mapper = new ObjectMapper();
        Customer customerAfterGetIdMethod = mapper.readValue(responseText, Customer.class);
        customerAfterGetIdMethod.setId(customerBeforeCreateMethod.getId());

        Logger.debug("customerBeforeCreateMethod:\n"+customerBeforeCreateMethod.toString());
        Logger.debug("customerAfterGetIdMethod:\n"+customerAfterGetIdMethod.toString());
        Assert.assertEquals(customerBeforeCreateMethod, customerAfterGetIdMethod);
        Logger.info("\n");
    }*/

    @Test(description = "Remove created customer.", dependsOnMethods = "createCustomer2")
    public void removeCustomer() throws IOException {
        String responseText = sendJsonRequest("remove_customer/"+customerBeforeCreateMethod.getLogin(),
                "admin","setup", "post", "");

        ObjectMapper mapper = new ObjectMapper();
        ResponseMessage respMessage = mapper.readValue(responseText, ResponseMessage.class);

        Assert.assertEquals("Customer "+customerBeforeCreateMethod.getLogin()+" was removed.",
                respMessage.getMessage());
    }
}
