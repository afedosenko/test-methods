package ru.nsu.fit.data;

public class ResponseMessage {

    private String message;

    public ResponseMessage setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getMessage() {
        return this.message;
    }

}
