package ru.nsu.fit.endpoint.service.database;

import jersey.repackaged.com.google.common.collect.Lists;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.Subscription;

import java.sql.*;
import java.util.List;
import java.util.UUID;

public class DBService {
    // Constants
    private static final String INSERT_CUSTOMER = "INSERT INTO CUSTOMER(id, first_name, last_name, login, pass, balance) values ('%s', '%s', '%s', '%s', '%s', %s)";
    private static final String INSERT_SUBSCRIPTION = "INSERT INTO SUBSCRIPTION(id, customer_id, plan_id) values ('%s', '%s', '%s')";
    private static final String INSERT_PLAN = "INSERT INTO PLAN(id, name, details, fee) values ('%s', '%s', '%s', %s)";

    private static final String SELECT_CUSTOMER_BY_LOGIN = "SELECT * FROM CUSTOMER WHERE login='%s'"; //replace id with *?
    private static final String SELECT_CUSTOMERS = "SELECT * FROM CUSTOMER";
    private static final String SELECT_ALL_PLANS = "SELECT * FROM PLAN";
    private static final String SELECT_PLANS = "SELECT * FROM PLAN " +
            "JOIN SUBSCRIPTION" +
            "ON SUBSCRIPTION.plan_id == PLAN.id" +
            "where SUBSCRIPTION.customer_id != '%s'";

    private static final String SELECT_CUSTOMER_BY_ID = "SELECT * FROM CUSTOMER WHERE id = '%s'";
    private static final String SELECT_SUBSCRIPTION_BY_ID = "SELECT * FROM SUBSCRIPTION WHERE id = '%s'";
    private static final String SELECT_SUBSCRIPTION_BY_CUSTOMER_PLAN = "SELECT * FROM SUBSCRIPTION WHERE customer_id = '%s' AND plan_id = '%s'";
    private static final String SELECT_CUSTOMER_SUBSCRIPTIONS = "SELECT * FROM SUBSCRIPTION WHERE customer_id = '%s'";

    private static final String SELECT_PLAN_BY_ID = "SELECT * FROM PLAN WHERE id = '%s'";
    private static final String SELECT_PLAN_BY_NAME = "SELECT * FROM PLAN WHERE name='%s'";

    private static final String DELETE_SUBSCRIPTION = "DELETE FROM SUBSCRIPTION WHERE id = '%s'";
    private static final String DELETE_PLAN = "DELETE FROM PLAN WHERE id = '%s'";
    private static final String DELETE_CUSTOMER = "DELETE FROM CUSTOMER WHERE id = '%s'";

    private static final String UPDATE_CUSTOMER = "UPDATE CUSTOMER SET first_name = '%s', last_name = '%s', balance = '%s' WHERE id = '%s'";
    private static final String UPDATE_PLAN = "UPDATE PLAN SET name = '%s', details = '%s', fee = '%s' WHERE id = '%s'";


    private Logger logger;
    private static final Object generalMutex = new Object();
    private Connection connection;

    public DBService(Logger logger) {
        this.logger = logger;
        init();
    }

    public Customer createCustomer(Customer customerData) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'createCustomer' was called with data: '%s'", customerData));

            customerData.setId(UUID.randomUUID());
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INSERT_CUSTOMER,
                                customerData.getId(),
                                customerData.getFirstName(),
                                customerData.getLastName(),
                                customerData.getLogin(),
                                customerData.getPass(),
                                customerData.getBalance()));
                return customerData;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public Customer updateCustomer(Customer customerData) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'updateCustomer' was called with data: '%s'", customerData));

            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                UPDATE_CUSTOMER,
                                customerData.getFirstName(),
                                customerData.getLastName(),
                                customerData.getBalance(),
                                customerData.getId()));
                return customerData;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public List<Customer> getCustomers() { //be careful! this method doesn't return customers' id's!!!
        synchronized (generalMutex) {
            logger.info("Method 'getCustomers' was called.");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(SELECT_CUSTOMERS);
                List<Customer> result = Lists.newArrayList();
                while (rs.next()) {
                    Customer customerData = new Customer()
                            .setFirstName(rs.getString(2))
                            .setLastName(rs.getString(3))
                            .setLogin(rs.getString(4))
                            .setPass(rs.getString(5))
                            .setBalance(rs.getInt(6));

                    result.add(customerData);
                }
                return result;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public Customer getCustomerById(UUID id) {
        synchronized (generalMutex) {
            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER_BY_ID,
                                id
                        )
                );
                Customer customerData = null;
                if (rs.next()) {
                    customerData = new Customer()
                            .setFirstName(rs.getString(2))
                            .setLastName(rs.getString(3))
                            .setLogin(rs.getString(4))
                            .setPass(rs.getString(5))
                            .setBalance(rs.getInt(6));
                }
                return customerData;
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public Customer getCustomerByLogin(String customerLogin) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'getCustomerByLogin' was called with data '%s'.", customerLogin));

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER_BY_LOGIN,
                                customerLogin));
                Customer customerData = null;
                if (rs.next()) {
                    customerData = new Customer()
                            .setId(UUID.fromString(rs.getString(1)))
                            .setFirstName(rs.getString(2))
                            .setLastName(rs.getString(3))
                            .setLogin(rs.getString(4))
                            .setPass(rs.getString(5))
                            .setBalance(Integer.parseInt(rs.getString(6)));
                }
                return customerData;
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public void removeCustomer(UUID customerId) {
        synchronized (generalMutex){
            logger.info(String.format("Method 'removeCustomer' was called with data '%s'.", customerId));
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                DELETE_CUSTOMER,
                                customerId
                        )
                );
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public Plan createPlan(Plan plan) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'createPlan' was called with data '%s'.", plan));

            plan.setId(UUID.randomUUID());
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INSERT_PLAN,
                                plan.getId(),
                                plan.getName(),
                                plan.getDetails(),
                                plan.getFee()));
                return plan;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public Plan updatePlan(Plan planData) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'updatePlan' was called with data: '%s'", planData));

            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                UPDATE_PLAN,
                                planData.getName(),
                                planData.getDetails(),
                                planData.getFee(),
                                planData.getId()));
                return planData;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public List<Plan> getAllPlans() { //be careful! this method doesn't return plans' id's!!!
        synchronized (generalMutex) {
            logger.info("Method 'getAllPlans' was called.");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(SELECT_ALL_PLANS);
                List<Plan> result = Lists.newArrayList();
                while (rs.next()) {
                    Plan planData = new Plan()
                            .setName(rs.getString(2))
                            .setDetails(rs.getString(3))
                            .setFee(rs.getInt(4));

                    result.add(planData);
                }
                return result;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public List<Plan> getPlans(UUID customerId) {
        synchronized (generalMutex) {
            logger.info("Method 'getPlans' was called.");

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(String.format(SELECT_PLANS, customerId));
                List<Plan> result = Lists.newArrayList();
                while (rs.next()) {
                    Plan planData = new Plan()
                            .setName(rs.getString(2))
                            .setDetails(rs.getString(3))
                            .setFee(Integer.parseInt(rs.getString(4)));
                    result.add(planData);
                }
                return result;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public Plan getPlanById(UUID id) {
        synchronized (generalMutex) {
            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_PLAN_BY_ID,
                                id
                        )
                );
                Plan planData = null;
                if (rs.next()) {
                    planData = new Plan()
                            .setName(rs.getString(2))
                            .setDetails(rs.getString(3))
                            .setFee(Integer.parseInt(rs.getString(4)));
                }
                return planData;
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public Plan getPlanByName(String name) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'getPlanByName' was called with data '%s'.", name));

            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_PLAN_BY_NAME,
                                name));
                Plan planData = null;
                if (rs.next()) {
                    planData = new Plan()
                            .setId(UUID.fromString(rs.getString(1)))
                            .setName(rs.getString(2))
                            .setDetails(rs.getString(3))
                            .setFee(Integer.parseInt(rs.getString(4)));
                }
                return planData;
            } catch (SQLException ex) {
                logger.debug(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public void removePlan(UUID planId) {
        synchronized (generalMutex){
            logger.info(String.format("Method 'removePlan' was called with data '%s'.", planId));
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                DELETE_PLAN,
                                planId
                        )
                );
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public Subscription createSubscription(Subscription subscription) {
        synchronized (generalMutex) {
            logger.info(String.format("Method 'createSubscription' was called with data '%s'.", subscription));

            subscription.setId(UUID.randomUUID());
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                INSERT_SUBSCRIPTION,
                                subscription.getId(),
                                subscription.getCustomerId(),
                                subscription.getPlanId()));
                return subscription;
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    public Subscription getSubscriptionById(UUID id){
        synchronized (generalMutex) {
            try{
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_SUBSCRIPTION_BY_ID,
                                id
                        )
                );
                Subscription subscriptionData = null;
                if(rs.next()){
                    subscriptionData = new Subscription()
                            .setId(UUID.fromString(rs.getString(1)))
                            .setCustomerId(UUID.fromString(rs.getString(2)))
                            .setPlanId(UUID.fromString(rs.getString(3)));
                }
                return  subscriptionData;
            }
            catch (SQLException ex){
                throw new RuntimeException(ex);
            }
        }
    }

    public Subscription getSubscriptionByPlanAndCustomer(UUID customerId, UUID planId){
        synchronized (generalMutex) {
            try{
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_SUBSCRIPTION_BY_CUSTOMER_PLAN,
                                customerId, planId
                        )
                );
                Subscription subscriptionData = null;
                if(rs.next()){
                    subscriptionData = new Subscription()
                            .setId(UUID.fromString(rs.getString(1)))
                            .setCustomerId(UUID.fromString(rs.getString(2)))
                            .setPlanId(UUID.fromString(rs.getString(3)));
                }
                return  subscriptionData;
            }
            catch (SQLException ex){
                throw new RuntimeException(ex);
            }
        }
    }

    public List<Subscription> getCustomerSubscriptions(UUID customerId) {
        synchronized (generalMutex) {
            try {
                Statement statement = connection.createStatement();
                ResultSet rs = statement.executeQuery(
                        String.format(
                                SELECT_CUSTOMER_SUBSCRIPTIONS,
                                customerId
                        )
                );
                List<Subscription> result = Lists.newArrayList();
                while (rs.next()) {
                    Subscription subscriptionData = new Subscription()
                            .setId(UUID.fromString(rs.getString(1)))
                            .setCustomerId(UUID.fromString(rs.getString(2)))
                            .setPlanId(UUID.fromString(rs.getString(3)));
                    result.add(subscriptionData);
                }
                return result;
            } catch (SQLException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public void removeSubscription(UUID subscriptionId) {
        synchronized (generalMutex){
            logger.info(String.format("Method 'removeSubscription' was called with data '%s'.", subscriptionId));
            try {
                Statement statement = connection.createStatement();
                statement.executeUpdate(
                        String.format(
                                DELETE_SUBSCRIPTION,
                                subscriptionId
                        )
                );
            } catch (SQLException ex) {
                logger.error(ex.getMessage(), ex);
                throw new RuntimeException(ex);
            }
        }
    }

    private void init() {
        logger.debug("-------- MySQL JDBC Connection Testing ------------");
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            logger.debug("Where is your MySQL JDBC Driver?", ex);
            throw new RuntimeException(ex);
        }

        logger.debug("MySQL JDBC Driver Registered!");

        try {
            connection = DriverManager
                    .getConnection(
                            "jdbc:mysql://localhost:3306/testmethods?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false",
                            "user",
                            "user");
        } catch (SQLException ex) {
            logger.error("Connection Failed! Check output console", ex);
            throw new RuntimeException(ex);
        }

        if (connection != null) {
            logger.debug("You made it, take control your database now!");
        } else {
            logger.error("Failed to make connection!");
        }
    }
}
