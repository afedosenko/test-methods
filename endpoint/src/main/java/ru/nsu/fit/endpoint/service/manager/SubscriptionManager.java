package ru.nsu.fit.endpoint.service.manager;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Subscription;

import java.util.List;
import java.util.UUID;

public class SubscriptionManager extends ParentManager {
    public SubscriptionManager(DBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    /**
     * Метод создает подписку. Ограничения:
     * 1. Подписки с таким планом пользователь не имеет.
     * 2. Стоймость подписки не превышает текущего баланса кастомера и после покупки вычитается из его баласа.
     */
    public Subscription createSubscription(Subscription subscription) {
        Validate.notNull(subscription, "Argument 'subscriptionData' is null.");
        Validate.notNull(subscription.getCustomerId(), "Customer is null.");
        Validate.notNull(subscription.getPlanId(), "Plan is null");
        CustomerManager customerManager = new CustomerManager(dbService, flowLog);
        PlanManager planManager = new PlanManager(dbService, flowLog);
        Validate.isTrue(customerManager.getCustomer(subscription.getCustomerId()).getBalance()
                >= planManager.getPlanById(subscription.getPlanId()).getFee(),
                "Customer has not enough money to get this plan.");
        Validate.isTrue(this.getSubscriptionByCustomerAndPlan(subscription.getCustomerId(), subscription.getPlanId()) == null,
                "Customer already subscripted on this plan.");
        return dbService.createSubscription(subscription);

    }

    public Subscription getSubscriptionByCustomerAndPlan(UUID customerId, UUID planId){
        return  dbService.getSubscriptionByPlanAndCustomer(customerId, planId);
    }

    public void removeSubscription(UUID subscriptionId) {
        dbService.removeSubscription(subscriptionId);
    }

    /**
     * Возвращает список подписок указанного customer'а.
     */
    public List<Subscription> getSubscriptions(UUID customerId) {
        return  dbService.getCustomerSubscriptions(customerId);
    }
}
