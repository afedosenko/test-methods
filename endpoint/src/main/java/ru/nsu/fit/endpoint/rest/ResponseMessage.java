package ru.nsu.fit.endpoint.rest;

public class ResponseMessage {

    private String message;

    public ResponseMessage setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getMessage() {
        return this.message;
    }

}
