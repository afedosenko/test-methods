package ru.nsu.fit.endpoint.service.manager;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Plan;

import java.util.List;
import java.util.UUID;

public class PlanManager extends ParentManager {
    public PlanManager(DBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    /**
     * Метод создает новый объект типа Plan. Ограничения:
         * name - длина не больше 128 символов и не меньше 2 включительно не содержит спец символов. Имена не пересекаются друг с другом;
    /* details - длина не больше 1024 символов и не меньше 1 включительно;
    /* fee - больше либо равно 0 но меньше либо равно 999999.
     */
    public Plan createPlan(Plan plan) {
        Validate.notNull(plan, "Argument 'plan' is null.");

        Validate.notNull(plan.getDetails(), "Details is null.");
        Validate.isTrue(!plan.getDetails().isEmpty(), "Details is empty.");
        Validate.isTrue(plan.getDetails().length() <= 1024,
                "Details's length should be less or equal 1024 symbols.");

        Validate.isTrue(plan.getFee() >= 0 && plan.getFee() <= 999999,
                "Fee should be more or equal 0 and less or equal 999999.");

        Validate.isTrue(plan.getName().length() >= 2 && plan.getName().length() <= 128,
                "Name's length should be more or equal 1 symbol and less or equal 1024 symbols.");

        boolean containsSpecSymbols = false;
        String name = plan.getName();
        for (int i=0; i<name.length(); i++) {
            char ch = name.charAt(i);
            if (! (Character.isLetterOrDigit(ch) || Character.isWhitespace(ch)) ) {
                containsSpecSymbols = true;
                break;
            }
        }
        Validate.isTrue(!containsSpecSymbols, "Name shouldn't contain spec symbols");

        Validate.isTrue(this.getPlanByName(plan.getName()) == null,
                "Plan with such name already exists.");

        return dbService.createPlan(plan); //where is dbService object defined?
    }

    public Plan getPlanById(UUID id){
        return  dbService.getPlanById(id);
    }

    public Plan getPlanByName(String name) {
        return dbService.getPlanByName(name);
    }

    public Plan updatePlan(Plan plan) {
        Validate.notNull(plan, "Argument 'plan' is null.");
        Validate.notNull(plan.getDetails(), "Details is null.");
        Validate.isTrue(!plan.getDetails().isEmpty(), "Details is empty.");
        Validate.isTrue(plan.getDetails().length() <= 1024,
                "Details's length should be less or equal 1024 symbols.");
        Validate.isTrue(plan.getFee() >= 0 && plan.getFee() <= 999999,
                "Fee should be more or equal 0 and less or equal 999999.");
        Validate.isTrue(plan.getName().length() >= 2 && plan.getName().length() <= 128,
                "Name's length should be more or equal 1 symbol and less or equal 1024 symbols.");

        return  dbService.updatePlan(plan);
    }

    public void removePlan(UUID id) {
        dbService.removePlan(id);
    }

    /**
     * Метод возвращает список планов доступных для покупки.
     */

    public List<Plan> getAllPlans() { //be careful! this method doesn't return plans' id's!!!
        return  dbService.getAllPlans();
    }

    public List<Plan> getPlans(UUID customerId) { //I think it should be renamed into getPlansForCustomer
        return  dbService.getPlans(customerId);
    }

}
