package ru.nsu.fit.endpoint.rest;

import org.apache.commons.lang.Validate;
import org.apache.commons.lang.exception.ExceptionUtils;
import ru.nsu.fit.endpoint.service.MainFactory;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.Subscription;
import ru.nsu.fit.endpoint.service.manager.CustomerManager;
import ru.nsu.fit.endpoint.shared.JsonMapper;

import javax.annotation.Generated;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path("")
public class RestService {

    @RolesAllowed({AuthenticationFilter.UNKNOWN, AuthenticationFilter.CUSTOMER, AuthenticationFilter.ADMIN})
    @GET
    @Path("/health_check")
    public Response healthCheck() {
        return Response.ok().entity("{\"status\": \"OK\"}").build();
    }

    @RolesAllowed({AuthenticationFilter.UNKNOWN, AuthenticationFilter.CUSTOMER, AuthenticationFilter.ADMIN})
    @GET
    @Path("/get_role")
    public Response getRole(@Context ContainerRequestContext crc) {
        return Response.ok().entity(String.format("{\"role\": \"%s\"}", crc.getProperty("ROLE"))).build();
    }

    @RolesAllowed(AuthenticationFilter.ADMIN)
    @GET
    @Path("/get_customers")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getCustomers() {
        try {
            List<Customer> customers = MainFactory.getInstance().getCustomerManager().getCustomers();
            return Response.ok().entity(JsonMapper.toJson(customers, true)).build();
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
            return Response.serverError().entity(JsonMapper.toJson(new ResponseMessage().setMessage(ex.getMessage()),
                    true)).build();
        }
    }

    @RolesAllowed(AuthenticationFilter.ADMIN)
    @POST
    @Path("/create_customer")
    @Consumes(MediaType.APPLICATION_JSON)
    //produces?
    public Response createCustomer(String customerDataJson) {
        try {
            // convert json to object
            Customer customerData = JsonMapper.fromJson(customerDataJson, Customer.class);

            // create new customer
            Customer customer = MainFactory.getInstance().getCustomerManager().createCustomer(customerData);

            // send the answer
            return Response.ok().entity(JsonMapper.toJson(customer, true)).build();
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
            return Response.serverError().entity(JsonMapper.toJson(new ResponseMessage().setMessage(ex.getMessage()),
                    true)).build();
        }
    }

    @RolesAllowed(AuthenticationFilter.ADMIN)
    @GET
    @Path("/get_customer_id/{customer_login}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerId(@Context ContainerRequestContext crc, @PathParam("customer_login") String customerLogin) {
        System.out.println("login="+customerLogin);
        System.out.println("crc username="+crc.getProperty("username"));
        System.out.println("crc role="+crc.getProperty("ROLE"));
        if(crc.getProperty("ROLE") == AuthenticationFilter.CUSTOMER && !crc.getProperty("username").equals(customerLogin)){
            return  Response.status(Response.Status.FORBIDDEN).entity("You can't access this user info.").build();
        }
        try {
            List<Customer> customers = MainFactory
                    .getInstance()
                    .getCustomerManager()
                    .getCustomers()
                    .stream()
                    .filter(x -> x.getLogin().equals(customerLogin))
                    .collect(Collectors.toList());

            Validate.isTrue(customers.size() == 1);
            return Response.ok().entity(JsonMapper.toJson(customers.get(0), true)).build();
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" +
                    ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

    @RolesAllowed({AuthenticationFilter.ADMIN})
    @POST
    @Path("/meow")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response meow(String json) {
        System.out.println("Entered meow method");
        return Response.ok().entity(JsonMapper.toJson("Meow.", true)).build();
    }

    @RolesAllowed({AuthenticationFilter.ADMIN})
    @POST
    @Path("/remove_customer/{customer_login}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response removeCustomer(@PathParam("customer_login") String customerLogin) {
        try {
            List<Customer> customers = MainFactory //is it necessary to write this test on 2 customers with same login?
                    .getInstance()
                    .getCustomerManager()
                    .getCustomers()
                    .stream()
                    .filter(x -> x.getLogin().equals(customerLogin))
                    .collect(Collectors.toList());
            Validate.isTrue(customers.size() == 1);

            Customer customer = MainFactory.getInstance().getCustomerManager()
                    .getCustomerByLogin(customers.get(0).getLogin()); //will we use this code alone, without test on 2 customers with same login?
            MainFactory.getInstance().getCustomerManager().removeCustomer(customer.getId());
            return Response.ok().entity(JsonMapper.toJson(
                    new ResponseMessage().setMessage("Customer "+customerLogin+" was removed."), true)).build();
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
            return Response.serverError().entity(JsonMapper.toJson(new ResponseMessage().setMessage(ex.getMessage()),
                    true)).build();
        }
    }

    @RolesAllowed(AuthenticationFilter.CUSTOMER)
    @GET
    @Path("/customer/get_plans")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerPlans(@Context ContainerRequestContext crc) {
        Customer customer = (Customer)crc.getProperty("customer");

        try {
            List<Plan> plans = MainFactory.getInstance().getPlanManager().getPlans(customer.getId());
            return Response.ok().entity(JsonMapper.toJson(plans, true)).build();
        } catch (IllegalArgumentException ex) {
            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" +
                    ExceptionUtils.getFullStackTrace(ex)).build();
        }
    }

//    @RolesAllowed(AuthenticationFilter.CUSTOMER)
//    @GET
//    @Path("/get_plan/{plan_name}")
//    @Consumes(MediaType.APPLICATION_JSON)
//    @Produces(MediaType.APPLICATION_JSON)
//    public Response getCustomerPlans(@PathParam("plan_name") String planName) {
//        try {
//            Plan plan = MainFactory.getInstance().getPlanManager().getPlanByName(planName);
//            return Response.ok().entity(JsonMapper.toJson(plan, true)).build();
//        } catch (IllegalArgumentException ex) {
//            return Response.status(Response.Status.BAD_REQUEST).entity(ex.getMessage() + "\n" +
//                    ExceptionUtils.getFullStackTrace(ex)).build();
//        }
//    }

    @RolesAllowed(AuthenticationFilter.ADMIN)
    @POST
    @Path("/create_plan")
    @Consumes(MediaType.APPLICATION_JSON)
    //produces?
    public Response createPlan(String planDataJson) {
        try {
            Plan planData = JsonMapper.fromJson(planDataJson, Plan.class);
            Plan plan = MainFactory.getInstance().getPlanManager().createPlan(planData);
            return Response.ok().entity(JsonMapper.toJson(plan, true)).build();
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
            return Response.serverError().entity(JsonMapper.toJson(new ResponseMessage().setMessage(ex.getMessage()),
                    true)).build();
        }
    }

    @RolesAllowed({AuthenticationFilter.ADMIN})
    @POST
    @Path("/remove_plan/{plan_name}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response removePlan(@PathParam("plan_name") String planName) {
        try {
            List<Plan> plans = MainFactory
                    .getInstance()
                    .getPlanManager()
                    .getAllPlans()
                    .stream()
                    .filter(x -> x.getName().equals(planName))
                    .collect(Collectors.toList());
            Validate.isTrue(plans.size() == 1);

            Plan plan = MainFactory.getInstance().getPlanManager()
                    .getPlanByName(plans.get(0).getName()); //will we use this code alone, without test on 2 plans with same name?
            MainFactory.getInstance().getPlanManager().removePlan(plan.getId());
            return Response.ok().entity(JsonMapper.toJson(
                    new ResponseMessage().setMessage("Plan "+planName+" was removed."), true)).build();
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
            return Response.serverError().entity(JsonMapper.toJson(new ResponseMessage().setMessage(ex.getMessage()),
                    true)).build();
        }
    }

    @RolesAllowed({AuthenticationFilter.ADMIN})
    @POST
    @Path("/create_subscription")
    @Consumes(MediaType.APPLICATION_JSON)
    //produces?
    public Response subscribeToPlan(@Context ContainerRequestContext crc, String subscriptionDataJson) {
        Customer customer = (Customer)crc.getProperty("customer");
        try {
            //move mainFactory to other place? is it okay to create mainfactory everywhere?
            Subscription subscriptionData = JsonMapper.fromJson(subscriptionDataJson, Subscription.class);
            if(crc.getProperty("ROLE") == AuthenticationFilter.CUSTOMER
                    && !(customer.getId().equals(subscriptionData.getCustomerId()))){
                return  Response.status(Response.Status.FORBIDDEN).entity("You can't access this user info.").build();
            }
            subscriptionData.setCustomerId(customer.getId());
            Subscription subscription = MainFactory.getInstance().getSubscriptionManager()
                    .createSubscription(subscriptionData);
            return Response.ok().entity(JsonMapper.toJson(subscription, true)).build();
        } catch (IllegalArgumentException ex) {
            ex.printStackTrace();
            return Response.serverError().entity(JsonMapper.toJson(new ResponseMessage().setMessage(ex.getMessage()),
                    true)).build();
        }
    }
}
