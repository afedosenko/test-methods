package ru.nsu.fit.endpoint.service.manager;

import org.apache.commons.lang.NotImplementedException;
import org.apache.commons.lang.Validate;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;

import java.util.List;
import java.util.UUID;

public class CustomerManager extends ParentManager {
    public CustomerManager(DBService dbService, Logger flowLog) {
        super(dbService, flowLog);
    }

    /**
     * Метод создает новый объект типа Customer. Ограничения:
     * Аргумент 'customerData' - не null;
     * firstName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов;
     * lastName - нет пробелов, длина от 2 до 12 символов включительно, начинается с заглавной буквы, остальные символы строчные, нет цифр и других символов;
     * login - указывается в виде email, проверить email на корректность, проверить что нет customer с таким же email;
     * pass - длина от 6 до 12 символов включительно, не должен быть простым (123qwe или 1q2w3e), не должен содержать части login, firstName, lastName
     * money - должно быть равно 0.
     */
    public Customer createCustomer(Customer customer) {
        Validate.notNull(customer, "Argument 'customerData' is null.");

        Validate.notNull(customer.getPass());
        Validate.isTrue(customer.getPass().length() >= 6 && customer.getPass().length() < 13, "Password's length should be more or equal 6 symbols and less or equal 12 symbols.");
        Validate.isTrue(!customer.getPass().equalsIgnoreCase("123qwe"), "Password is easy.");
        Validate.isTrue(customer.getFirstName().length() >= 2 && customer.getFirstName().length() <= 12, "Wrong First Name.");
        Validate.isTrue(Character.isUpperCase(customer.getFirstName().charAt(0)), "Wrong First Name.");
        Validate.isTrue(customer.getFirstName().substring(1).toLowerCase().equals(customer.getFirstName().substring(1)), "Wrong First Name.");
        Validate.isTrue(customer.getLastName().length() >= 2 && customer.getLastName().length() <= 12, "Wrong Last Name.");
        Validate.isTrue(Character.isUpperCase(customer.getLastName().charAt(0)), "Wrong Last Name.");
        Validate.isTrue(customer.getLastName().substring(1).toLowerCase().equals(customer.getLastName().substring(1)), "Wrong Last Name.");

        Validate.isTrue(this.getCustomerByLogin(customer.getLogin()) == null,
                "Customer with such login already exists.");
        // TODO: необходимо дописать дополнительные проверки

        return dbService.createCustomer(customer);
    }

    public Customer getCustomerByLogin(String login) {
        return dbService.getCustomerByLogin(login);
    }

    public List<Customer> getCustomers() { //be careful! this method doesn't return customers' id's!!!
        return dbService.getCustomers();
    }

    public Customer getCustomer(UUID id) {
        return dbService.getCustomerById(id);
    }


    /**
     * Метод обновляет объект типа Customer.
     * Можно обновить только firstName и lastName.
     */
    public Customer updateCustomer(Customer customer) {
        Customer currentCustomer = dbService.getCustomerById(customer.getId());
        Validate.isTrue(currentCustomer.getBalance() == customer.getBalance(), "Invalid fields.");
        Validate.isTrue(currentCustomer.getLogin() == customer.getLogin(), "Invalid fields.");
        Validate.isTrue(currentCustomer.getPass() == customer.getPass(), "Invalid fields.");
        Validate.isTrue(customer.getFirstName().length() >= 2 && customer.getFirstName().length() <= 12, "Wrong First Name.");
        Validate.isTrue(Character.isUpperCase(customer.getFirstName().charAt(0)), "Wrong First Name.");
        Validate.isTrue(customer.getFirstName().substring(1).toLowerCase().equals(customer.getFirstName().substring(1)), "Wrong First Name.");
        Validate.isTrue(customer.getLastName().length() >= 2 && customer.getLastName().length() <= 12, "Wrong Last Name.");
        Validate.isTrue(Character.isUpperCase(customer.getLastName().charAt(0)), "Wrong Last Name.");
        Validate.isTrue(customer.getLastName().substring(1).toLowerCase().equals(customer.getLastName().substring(1)), "Wrong Last Name.");
        return  dbService.updateCustomer(customer);
    }

    public void removeCustomer(UUID id) {
        dbService.removeCustomer(id);
    }

    /**
     * Метод добавляет к текущему баласу amount.
     * amount - должен быть строго больше нуля.
     */
    public Customer topUpBalance(UUID customerId, int amount) {
        Customer customer = dbService.getCustomerById(customerId);
        customer.setBalance(customer.getBalance() + amount);
        return dbService.updateCustomer(customer);
    }
}
