$(document).ready(function(){
    login = getCookie("login");
    pass = getCookie("pass");
    $.get({
        url: 'rest/get_plans',
        headers: {
            'Authorization': 'Basic ' + btoa(login + ':' + pass)
        }
    }).done(function(data) {
        var json = $.parseJSON(data);

        var dataSet = []
        for(var i = 0; i < json.length; i++) {
            var obj = json[i];
            dataSet.push([obj.id, obj.name, obj.fee, obj.details])
        }

        //$("#customer_list_id").html(data);
        $('#plan_list_id')
            .DataTable({
                data: dataSet,
                columns: [
                    { title: "Id"},
                    { title: "Name" },
                    { title: "Fee" },
                    { title: "Details" },
                    { mRender: function(data, type, row) {
                                            return '<button class="subscribe_plan" plan_id="' + row.Id  + '" onclick="subscribePlan(\'' + row.Id + '\')">Subscribe</button>'
                                        }
                ]
            });
    });

});
function subscribePlan(plan_id) {
    $.post({
        url: 'rest/create_subscription,
        headers: {
            'Authorization': 'Basic ' + btoa('admin' + ':' + 'setup'),
            'Content-Type': 'application/json'
        }
        data: {
            'customer_id': null,
            'plan_id': plan_id
        }
    }).always(function(data) {
        $.redirect('/endpoint/customer_all_plans.html', {'login': 'admin', 'pass': 'setup', 'role': 'ADMIN'}, 'GET');
    });
}