package ru.nsu.fit.endpoint.service.manager;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Plan;

import java.util.UUID;

public class PlanManagerTest {
    private DBService dbService;
    private Logger logger;
    private PlanManager planManager;

    private Plan planBeforeCreateMethod;
    private Plan planAfterCreateMethod;

    @Before
    public void before() {
        dbService = Mockito.mock(DBService.class);
        logger = Mockito.mock(Logger.class);

        planBeforeCreateMethod = new Plan()
                .setId(null)
                .setName("Plan 1")
                .setDetails("This plan has no details")
                .setFee(50);
        planAfterCreateMethod = planBeforeCreateMethod.clone();
        planAfterCreateMethod.setId(UUID.randomUUID());

        Mockito.when(dbService.createPlan(planBeforeCreateMethod)).thenReturn(planAfterCreateMethod);

        planManager = new PlanManager(dbService, logger);
    }

    @Test
    public void testCreateNewPlan() {
        Mockito.when(dbService.getPlanByName(planBeforeCreateMethod.getName())).thenReturn(null);
        Plan plan = planManager.createPlan(planBeforeCreateMethod);
        Assert.assertEquals(plan.getId(), planAfterCreateMethod.getId());
        Assert.assertEquals(2, Mockito.mockingDetails(dbService).getInvocations().size());
    }

    @Test
    public void testCreatePlanWithNullArgument() {
        try {
            planManager.createPlan(null);
            Assert.fail("Created plan with null argument");
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Argument 'plan' is null.", ex.getMessage());
        }
    }

    @Test
    public void testCreatePlanWithNullDetails() {
        try {
            planBeforeCreateMethod.setDetails(null);
            planManager.createPlan(planBeforeCreateMethod);
            Assert.fail("Created plan with null 'details' field");
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Details is null.", ex.getMessage());
        }
    }

    @Test
    public void testCreatePlanWithEmptyDetails() {
        try {
            planBeforeCreateMethod.setDetails("");
            planManager.createPlan(planBeforeCreateMethod);
            Assert.fail("Created plan with empty 'details' field");
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Details is empty.", ex.getMessage());
        }
    }

    @Test
    public void testCreatePlanWithLongDetails() {
        try {
            int len = 1025;
            StringBuilder s = new StringBuilder(len);
            for (int i = 0; i < len; i++) {
                s.append('a');
            }
            planBeforeCreateMethod.setDetails(s.toString());
            planManager.createPlan(planBeforeCreateMethod);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Details's length should be less or equal 1024 symbols.", ex.getMessage());
        }
    }

    @Test
    public void testCreatePlanWithMinLengthDetails() {
        planBeforeCreateMethod.setDetails("a");
        planManager.createPlan(planBeforeCreateMethod);
    }

    @Test
    public void testCreatePlanWithMaxLengthDetails() {
        int len = 1024;
        StringBuilder s = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            s.append('a');
        }
        planBeforeCreateMethod.setDetails(s.toString());
        planManager.createPlan(planBeforeCreateMethod);
    }

    @Test
    public void testCreatePlanWithNegativeFee() {
        try {
            planBeforeCreateMethod.setFee(-1);
            planManager.createPlan(planBeforeCreateMethod);
            Assert.fail("Created plan with negative fee");
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Fee should be more or equal 0 and less or equal 999999.", ex.getMessage());
        }
    }

    @Test
    public void testCreatePlanWithBigFee() {
        try {
            planBeforeCreateMethod.setFee(1000000);
            planManager.createPlan(planBeforeCreateMethod);
            Assert.fail("Created plan with too big fee");
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Fee should be more or equal 0 and less or equal 999999.", ex.getMessage());
        }
    }

    @Test
    public void testCreatePlanWithMinimalFee() {
        planBeforeCreateMethod.setFee(0);
        planManager.createPlan(planBeforeCreateMethod);
    }

    @Test
    public void testCreatePlanWithMaximalFee() {
        planBeforeCreateMethod.setFee(999999);
        planManager.createPlan(planBeforeCreateMethod);
    }

    @Test
    public void testCreatePlanWithShortName() {
        try {
            planBeforeCreateMethod.setName("a");
            planManager.createPlan(planBeforeCreateMethod);
            Assert.fail("Created plan with too short name");
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Name's length should be more or equal 1 symbol and less or equal 1024 symbols.",
                    ex.getMessage());

        }
    }

    @Test
    public void testCreatePlanWithLongName() {
        try {
            int len = 129;
            StringBuilder s = new StringBuilder(len);
            for (int i = 0; i < len; i++) {
                s.append('a');
            }
            planBeforeCreateMethod.setName(s.toString());
            planManager.createPlan(planBeforeCreateMethod);
            Assert.fail("Created plan with too long name");
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Name's length should be more or equal 1 symbol and less or equal 1024 symbols.",
                    ex.getMessage());

        }
    }

    @Test
    public void testCreatePlanWithSpecSymbol() {
        try {
            planBeforeCreateMethod.setName("Plan #1");
            planManager.createPlan(planBeforeCreateMethod);
            Assert.fail("Created plan with spec symbol in name");
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Name shouldn't contain spec symbols", ex.getMessage());
        }
    }

    @Test
    public void testCreatePlanWithExistingName() {
        try {
            planManager.createPlan(planBeforeCreateMethod);
            Mockito.when(dbService.getPlanByName(planBeforeCreateMethod.getName())).thenReturn(planAfterCreateMethod);
            planManager.createPlan(planBeforeCreateMethod);
            Assert.fail("Created two plans with same name");
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Plan with such name already exists.", ex.getMessage());
        }
    }

    @Test
    public void testCreatePlanWithMinLengthName() {
        planBeforeCreateMethod.setName("aa");
        planManager.createPlan(planBeforeCreateMethod);
    }

    @Test
    public void testCreatePlanWithMaxLengthName() {
        int len = 128;
        StringBuilder s = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            s.append('a');
        }
        planBeforeCreateMethod.setName(s.toString());
        planManager.createPlan(planBeforeCreateMethod);
    }

}
