package ru.nsu.fit.endpoint.service.manager;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.slf4j.Logger;
import ru.nsu.fit.endpoint.service.database.DBService;
import ru.nsu.fit.endpoint.service.database.data.Customer;
import ru.nsu.fit.endpoint.service.database.data.Plan;
import ru.nsu.fit.endpoint.service.database.data.Subscription;

import java.util.UUID;

public class SubscriptionManagerTest {
    private DBService dbService;
    private Logger logger;
    private CustomerManager customerManager;
    private SubscriptionManager subscriptionManager;
    private PlanManager planManager;
    private Customer customer;
    private Plan plan;
    private Subscription subscriptionBefore;
    private Subscription subscriptionAfter;

    @Before
    public void before() {
        // create stubs for the test's class
        dbService = Mockito.mock(DBService.class);
        logger = Mockito.mock(Logger.class);
        customer = new Customer().setId(UUID.randomUUID())
                .setFirstName("Alexey")
                .setLastName("Fed")
                .setBalance(1000)
                .setLogin("afedosenko")
                .setPass("mypass123");
        plan = new Plan().setId(UUID.randomUUID()).setDetails("My Plan details").setFee(100).setName("Plan");
        subscriptionBefore = new Subscription().setCustomerId(customer.getId()).setPlanId(plan.getId());
        subscriptionAfter = subscriptionBefore.clone();
        subscriptionAfter.setId(UUID.randomUUID());

        Mockito.when(dbService.createSubscription(subscriptionBefore)).thenReturn(subscriptionAfter);
        Mockito.when(dbService.getPlanById(plan.getId())).thenReturn(plan);
        Mockito.when(dbService.getCustomerById(customer.getId())).thenReturn(customer);

        // create the test's class
        customerManager = new CustomerManager(dbService, logger);
        planManager = new PlanManager(dbService, logger);
        subscriptionManager = new SubscriptionManager(dbService, logger);

    }

    @Test
    public void testCreateNewSubscription() {
        // Вызываем метод, который хотим протестировать
        plan.setFee(100);
        customer.setBalance(101);
        subscriptionBefore.setPlanId(plan.getId()).setCustomerId(customer.getId());
        Subscription subscription = subscriptionManager.createSubscription(subscriptionBefore);

        // Проверяем результат выполенния метода
        Assert.assertEquals(subscription.getId(), subscriptionAfter.getId());

        // Проверяем, что метод мока базы данных был вызван 1 раз
        Assert.assertEquals(4, Mockito.mockingDetails(dbService).getInvocations().size());
    }

    @Test
    public void testCreateSubscriptionWithNullArgument() {
        try {
            subscriptionManager.createSubscription(null);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Argument 'subscriptionData' is null.", ex.getMessage());
        }
    }

    @Test
    public void testCreateSubscriptionWithLowBalance() {
        try {
            customer.setBalance(100);
            plan.setFee(101);
            subscriptionManager.createSubscription(subscriptionBefore);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Customer has not enough money to get this plan.", ex.getMessage());
        }
    }
    @Test
    public void testCreateSubscriptionCustomerAlreadyHas() {
        try {
            customer.setBalance(101);
            plan.setFee(100);
//            subscriptionManager.createSubscription(subscriptionBefore);
            Mockito.when(dbService.getSubscriptionByPlanAndCustomer(customer.getId(), plan.getId())).thenReturn(subscriptionAfter);
            subscriptionManager.createSubscription(subscriptionBefore);
            Assert.fail("Created two subscriptions on one user.");
        } catch (IllegalArgumentException ex) {
            Assert.assertEquals("Customer already subscripted on this plan.", ex.getMessage());
        }
    }
}
